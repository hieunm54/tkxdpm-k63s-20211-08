PHÂN CÔNG CÔNG VIỆC: 

- vẽ các biểu đồ sequence diagram và class diagram cho:
UC thuê xe: Nguyễn Minh Hiếu
UC trả xe: Nguyễn Quang Hùng
UC thanh toán: Hoàng Nhật Minh
UC tìm kiếm bãi xe: Lê Văn Hoàng
UC quản lý xe: Nguyễn Minh Tuân
UC quả lý giá thuê xe: Trịnh Minh Tuấn

- Gộp các class bị trùng và thống nhất biểu đồ phân gói: cả nhóm

- Vẽ biểu đồ phân gói và tổng hợp báo cáo: Hoàng Nhật Minh

