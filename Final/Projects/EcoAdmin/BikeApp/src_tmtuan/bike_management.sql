-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 23, 2021 at 02:55 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bike_management`
--

-- --------------------------------------------------------

--
-- Table structure for table `rentalprice`
--

CREATE TABLE `rentalprice` (
  `BikeType` int(255) UNSIGNED NOT NULL,
  `Price15` double UNSIGNED NOT NULL,
  `Price30` double UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rentalprice`
--

INSERT INTO `rentalprice` (`BikeType`, `Price15`, `Price30`) VALUES
(0, 15000, 30000),
(1, 20000, 35000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rentalprice`
--
ALTER TABLE `rentalprice`
  ADD PRIMARY KEY (`BikeType`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
