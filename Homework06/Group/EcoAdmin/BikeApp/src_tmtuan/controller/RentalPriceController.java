package controller;

import model.RentalPrice;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RentalPriceController extends BaseController<RentalPrice> {
    public RentalPriceController(Class<RentalPrice> rentalPrice){
        super(rentalPrice);
    }

    public List<RentalPrice> get(){
        String sql = "Select * from rentalprice";

        List<RentalPrice> list = null;
        try {
            PreparedStatement pstm = conn.prepareStatement(sql);

            ResultSet rs = pstm.executeQuery();
            list = new ArrayList<RentalPrice>();
            while (rs.next()) {
                int BikeType = rs.getInt("BikeType");
                double Price15 = rs.getDouble("Price15");
                double Price30 = rs.getDouble("Price30");

                list.add(new RentalPrice(BikeType, Price15, Price30));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

}
