package model;

import annotaion.Key;

public class BikeType {

	@Key
	private int BikeType;

	private String BikeTypeName;
	
	public BikeType() {
		
	}

	public BikeType(int bikeTypeId, String bikeTypeName) {
		super();
		BikeType = bikeTypeId;
		BikeTypeName = bikeTypeName;
	}
	
	public String toString() {
		return BikeTypeName;
	}
	
	public int getBikeType() {
		return BikeType;
	}

	public void setBikeType(int bikeType) {
		BikeType = bikeType;
	}

	public String getBikeTypeName() {
		return BikeTypeName;
	}

	public void setBikeTypeName(String bikeTypeName) {
		BikeTypeName = bikeTypeName;
	}

}
