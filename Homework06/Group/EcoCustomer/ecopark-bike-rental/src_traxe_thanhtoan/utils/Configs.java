package utils;

import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
/**
 * @author nguyenlm Contains the configs for AIMS Project
 */
public class Configs {

	// api constants
	public static final String GET_BALANCE_URL = "https://ecopark-system-api.herokuapp.com/api/card/balance/118609_group1_2020";
	public static final String GET_VEHICLECODE_URL = "https://ecopark-system-api.herokuapp.com/api/get-vehicle-code/1rjdfasdfas";
	public static final String PROCESS_TRANSACTION_URL = "https://ecopark-system-api.herokuapp.com/api/card/processTransaction";
	public static final String RESET_URL = "https://ecopark-system-api.herokuapp.com/api/card/reset";

	// demo data
	public static final String POST_DATA = "{"
			+ " \"secretKey\": \"BUXj/7/gHHI=\" ,"
			+ " \"transaction\": {"
			+ " \"command\": \"pay\" ,"
			+ " \"cardCode\": \"118609_group1_2020\" ,"
			+ " \"owner\": \"Group 1\" ,"
			+ " \"cvvCode\": \"185\" ,"
			+ " \"dateExpried\": \"1125\" ,"
			+ " \"transactionContent\": \"Pei debt\" ,"
			+ " \"amount\": 50000 "
			+ "}"
		+ "}";
	public static final String TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiIxMTg2MDlfZ3JvdXAxXzIwMjAiLCJpYXQiOjE1OTkxMTk5NDl9.y81pBkM0pVn31YDPFwMGXXkQRKW5RaPIJ5WW5r9OW-Y";

	// database Configs
	public static final String DB_NAME = "aims";
	public static final String DB_USERNAME = System.getenv("DB_USERNAME");
	public static final String DB_PASSWORD = System.getenv("DB_PASSWORD");

	public static String CURRENCY = "VND";
	public static float PERCENT_VAT = 10;

	// static resource
	public static final String IMAGE_PATH = "assets/images";
	
	public static final String RESULT_SCREEN_PATH = "/views/fxml/result.fxml";
	public static final String SPLASH_SCREEN_PATH = "/views/fxml/splash.fxml";



	public static final String HOME_PATH  = "/views/fxml/home.fxml";

	public static final String POPUP_PATH = "/views/fxml/popup.fxml";
	
	public static final String RETURN_BIKE_SCREEN_PATH = "/views/fxml/return_bike.fxml";
	public static final String RENTING_BIKE_SCREEN_PATH = "/views/fxml/renting.fxml";
	public static final String PAYMENT_SCREEN_PATH = "/views/fxml/payment.fxml";

	public static Font REGULAR_FONT = Font.font("Segoe UI", FontWeight.NORMAL, FontPosture.REGULAR, 24);

	public static String[] PROVINCES = { "Báº¯c Giang", "Báº¯c Káº¡n", "Cao Báº±ng", "HÃ  Giang", "Láº¡ng SÆ¡n", "PhÃº Thá»�",
			"Quáº£ng Ninh", "ThÃ¡i NguyÃªn", "TuyÃªn Quang", "YÃªn BÃ¡i", "Ä�iá»‡n BiÃªn", "HÃ²a BÃ¬nh", "Lai ChÃ¢u", "SÆ¡n La",
			"Báº¯c Ninh", "HÃ  Nam", "Háº£i DÆ°Æ¡ng", "HÆ°ng YÃªn", "Nam Ä�á»‹nh", "Ninh BÃ¬nh", "ThÃ¡i BÃ¬nh", "VÄ©nh PhÃºc", "HÃ  Ná»™i",
			"Háº£i PhÃ²ng", "HÃ  TÄ©nh", "Nghá»‡ An", "Quáº£ng BÃ¬nh", "Quáº£ng Trá»‹", "Thanh HÃ³a", "Thá»«a ThiÃªn-Huáº¿", "Ä�áº¯k Láº¯k",
			"Ä�áº¯k NÃ´ng", "Gia Lai", "Kon Tum", "LÃ¢m Ä�á»“ng", "BÃ¬nh Ä�á»‹nh", "BÃ¬nh Thuáº­n", "KhÃ¡nh HÃ²a", "Ninh Thuáº­n",
			"PhÃº YÃªn", "Quáº£ng Nam", "Quáº£ng NgÃ£i", "Ä�Ã  Náºµng", "BÃ  Rá»‹a-VÅ©ng TÃ u", "BÃ¬nh DÆ°Æ¡ng", "BÃ¬nh PhÆ°á»›c", "Ä�á»“ng Nai",
			"TÃ¢y Ninh", "Há»“ ChÃ­ Minh", "An Giang", "Báº¡c LiÃªu", "Báº¿n Tre", "CÃ  Mau", "Ä�á»“ng ThÃ¡p", "Háº­u Giang",
			"KiÃªn Giang", "Long An", "SÃ³c TrÄƒng", "Tiá»�n Giang", "TrÃ  Vinh", "VÄ©nh Long", "Cáº§n ThÆ¡" };
}
