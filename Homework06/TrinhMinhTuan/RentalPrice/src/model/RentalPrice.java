package model;

import annotation.Key;

public class RentalPrice {
    @Key
    private int BikeType;
    private double Price15;
    private double Price30;

    public RentalPrice() {

    }

    public RentalPrice(int bikeType, double price15, double price30) {
        super();
        BikeType = bikeType;
        Price15 = price15;
        Price30 = price30;
    }

    public int getBikeType() {
        return BikeType;
    }

    public void setBikeType(int bikeType) {
        BikeType = bikeType;
    }

    public double getPrice15() {
        return Price15;
    }

    public void setPrice15(double price15) {
        Price15 = price15;
    }

    public double getPrice30() {
        return Price30;
    }

    public void setPrice30(double price30) {
        Price30 = price30;
    }
}
