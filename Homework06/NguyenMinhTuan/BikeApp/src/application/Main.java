package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("/views/fxml/BikeControl.fxml"));
//			BorderPane root = new BorderPane();
			Scene scene = new Scene(root,1366,768);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
//		Bike bike = new Bike(1, "Asama", 1, "Xe thể thao", "Đỏ");
//		
//		BikeController bikeController = new BikeController(Bike.class);
//		
//		System.out.println(bikeController.get());
	}
}
