package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import db.ConnectionUtils;
import model.Bike;
import model.BikeType;
import sources.enums.ActionStatus;

public class BikeController extends BaseController<Bike> {

	public BikeController(Class<Bike> bike) {
		// TODO Auto-generated constructor stub
		super(bike);
	}
	
	
	/**
	 * Hàm lấy dữ liệu
	 * @return
	 */
	public List<Bike> get() {
		
		String sql = "Select * from bike b, bikeType bt where b.bikeType = bt.bikeType order by BikeCode asc";

		List<Bike> list = null;
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);

			ResultSet rs = pstm.executeQuery();
			list = new ArrayList<Bike>();
			while (rs.next()) {
				int id = rs.getInt("BikeId");
				String bikeCode = rs.getString("BikeCode");
				String name = rs.getString("BikeName");
				int bikeType = rs.getInt("BikeType");
				String bikeTypeName = rs.getString("BikeTypeName");
				String color = rs.getString("Color");
				list.add(new Bike(id, bikeCode, name, bikeType, bikeTypeName, color));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	/**
	 * Hàm lấy dữ liệu
	 * @return
	 */
	public List<BikeType> getBikeType() {
		
		String sql = "Select * from bikeType";

		List<BikeType> list = null;
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);

			ResultSet rs = pstm.executeQuery();
			list = new ArrayList<BikeType>();
			while (rs.next()) {
				int id = rs.getInt("BikeType");
				String name = rs.getString("BikeTypeName");
				list.add(new BikeType(id, name));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	/**
	 * Hàm lấy dữ liệu
	 * @return
	 */
	public List<Bike> getCodeDuplicate(String code, int bikeId, ActionStatus status) {
		
		String sql = "";
		if (status == ActionStatus.ADD) {
			sql = "Select * from Bike where BikeCode = ?";
		} else if (status == ActionStatus.UPDATE) {
			sql = "Select * from Bike b where b.BikeCode = ? and b.BikeId <> ?";
		}

		List<Bike> list = null;
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);
			
			pstm.setString(1, code);
			if (status == ActionStatus.UPDATE) {
				pstm.setInt(2, bikeId);				
			}


			ResultSet rs = pstm.executeQuery();
			list = new ArrayList<Bike>();
			while (rs.next()) {
				int id = rs.getInt("BikeId");
				String bikeCode = rs.getString("BikeCode");
				String name = rs.getString("BikeName");
				int bikeType = rs.getInt("BikeType");
				String bikeTypeName = "";
				String color = rs.getString("Color");
				list.add(new Bike(id, bikeCode, name, bikeType, bikeTypeName, color));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;
	}
	
	/**
	 * Hàm validate
	 * @param entity
	 * @return
	 */
	public boolean validate(Bike bike, ActionStatus status) {
		
		String code = bike.getBikeCode();
		int id = bike.getBikeId();
		
		List<Bike> bikes = getCodeDuplicate(code, id, status);
		
		if (bikes.size() > 0) {
			return false;
		}

		return true;
	}

}
