package controller;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import annotaion.CustomData;
import annotaion.Key;
import annotaion.Unique;
import db.ConnectionUtils;
import model.Bike;
import sources.enums.ActionStatus;

public class BaseController<Entity> {
	
	protected Connection conn;
	protected Class<Entity> entityClass;

	public BaseController(Class<Entity> entityClass) {
		conn = ConnectionUtils.getConnection();
		this.entityClass = entityClass;
	}
	
	/**
	 * Hàm sinh sqlQuery khi insert
	 * @param entity
	 * @return
	 */
	public String genInsertQuery(Entity entity) {
		Class<?> entityClass = entity.getClass();
		Field[] allFields = entityClass.getDeclaredFields();
		
		List<Field> fieldLst = new ArrayList<Field>();
		
		for (int i = 0; i < allFields.length; i++ ) {
			Field field = allFields[i];
			Annotation customDataAnnotation = field.getAnnotation(CustomData.class);
			Annotation keyAnnotation = field.getAnnotation(Key.class);
			
			if (customDataAnnotation == null && keyAnnotation == null) {
				fieldLst.add(field);
			}
		}
		
		String query = "INSERT INTO ";
		
		query = query + entityClass.getSimpleName() + " (";
		
		for	(int i = 0; i < fieldLst.size(); i++) {
			String fieldName = fieldLst.get(i).getName();
			query += fieldName;
			
			if (i == fieldLst.size() - 1) {
				query += ") VALUES (";
			} else {
				query += ", ";
			}
		}
		
		for	(int i = 0; i < fieldLst.size(); i++) {
			String fieldName = fieldLst.get(i).getName();
			try {
				Method method = entityClass.getDeclaredMethod("get" + fieldName);
				Object value = method.invoke(entity);
				if (method.getReturnType() == String.class) {
					query = query + "'" + value +"'";	
				} else {
					query = query + value;					
				}
				if (i == fieldLst.size() - 1) {
					query += ");";
				} else {
					query += ", ";
				}
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
				
//		System.out.println(query);
		return query;
	}
	
	/**
	 * Hàm sinh sqlQuery khi update
	 * @param entity
	 * @param id
	 * @return
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 */
	public String genUpdateQuery(Entity entity) {
		Class<?> entityClass = entity.getClass();
		Field[] allFields = entityClass.getDeclaredFields();
		List<Field> fieldLst = new ArrayList<Field>();
		String keyFieldName = "";
		int keyValue = -1;
		
		for (int i = 0; i < allFields.length; i++ ) {
			Field field = allFields[i];
			Annotation customDataAnnotation = field.getAnnotation(CustomData.class);
			Annotation keyAnnotation = field.getAnnotation(Key.class);
			
			if (customDataAnnotation == null && keyAnnotation == null) {
				fieldLst.add(field);
			}
			
			if (keyAnnotation != null) {
				keyFieldName = field.getName();
				Method method;
				try {
					method = entityClass.getDeclaredMethod("get" + keyFieldName);
					keyValue = (int)method.invoke(entity);
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		if (keyValue < 0) {
			return "";
		}
		
		String query = "UPDATE ";
		
		query = query + entityClass.getSimpleName() + " SET ";
		
		for	(int i = 0; i < fieldLst.size(); i++) {
			Field field = fieldLst.get(i);
			
			String fieldName = field.getName();

			query += fieldName + " = ";
			
			try {
				Method method = entityClass.getDeclaredMethod("get" + fieldName);
				Object value = method.invoke(entity);
				
				if (method.getReturnType() == String.class) {
					query = query + "'" + value +"'";	
				} else {
					query = query + value;					
				}
				
				if (i != fieldLst.size() - 1) {
					query += ", ";
				} else {

				}
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}

		}
		
		query = query + " WHERE " + keyFieldName + " = " + keyValue;
				
//		System.out.println(query);
		return query;
	}
	
	/**
	 * Hàm thêm entity vào db
	 * @param entity
	 * @return
	 */
	public int add(Entity entity) {
		
		String sql = genInsertQuery(entity);
		int rowAffect = 0;

		try {
			PreparedStatement pstm = conn.prepareStatement(sql);

			rowAffect = pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffect;
	}
	
	/**
	 * Hàm cập nhật entity
	 * @param entity
	 * @return
	 */
	public int update(Entity entity) {
		String sql = genUpdateQuery(entity);
		int rowAffect = 0;

		try {
			PreparedStatement pstm = conn.prepareStatement(sql);

			rowAffect = pstm.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rowAffect;
	}
	
	/**
	 * Hàm xóa theo id
	 * @param id
	 * @return
	 */
	public int delete(int id) {
		Field[] fields = entityClass.getDeclaredFields(); 
		String keyFieldName = "";
		int rowAffect = 0;
		
		for (int i = 0; i < fields.length; i++ ) {
			Field field = fields[i];
			Annotation keyAnnotation = field.getAnnotation(Key.class);

			if (keyAnnotation != null) {
				keyFieldName = field.getName();
			}
		}
		
		String sql = "Delete from " + entityClass.getSimpleName() + " Where " + keyFieldName + " = ?";
		
		try {
			PreparedStatement pstm = conn.prepareStatement(sql);

			pstm.setInt(1, id);

			rowAffect = pstm.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rowAffect;
	}
	
	/**
	 * Hàm validate
	 * @param entity
	 * @return
	 */
	public boolean validate(Entity entity, ActionStatus status) {
		
		return true;
	}

}
