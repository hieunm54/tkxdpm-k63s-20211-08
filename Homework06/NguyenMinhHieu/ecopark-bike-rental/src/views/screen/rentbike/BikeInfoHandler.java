package views.screen.rentbike;

import entity.Bike;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class BikeInfoHandler {

    @FXML
    private Label textColor;

    @FXML
    private Label textPrice30Min;

    @FXML
    private Label textPriceEach15Min;

    public void initializeTextField(Bike bike){
        textColor.setText(bike.getColor());
        if(bike.getBikeType() == "Electric" || bike.getBikeType() == "Xe dap doi")	{
        	textPrice30Min.setText("15000d");
        	textPriceEach15Min.setText("22500d");
        }else {
        	textPrice30Min.setText("10000d");
        	textPriceEach15Min.setText("15000d");
        }
    }
}
