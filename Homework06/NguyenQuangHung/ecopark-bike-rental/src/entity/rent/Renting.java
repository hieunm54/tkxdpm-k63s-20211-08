package entity.rent;

import java.time.LocalDate;

import entity.bike.Bike;

public class Renting {
	// CRUD
	private int rentingStatus;
	private LocalDate startime;
	private LocalDate endDate;
	private int cost;
	
	public int getRentingStatus() {
		return rentingStatus;
	}

	public void setRentingStatus(int rentingStatus) {
		this.rentingStatus = rentingStatus;
	}

	public LocalDate getStartime() {
		return startime;
	}

	public void setStartime(LocalDate startime) {
		this.startime = startime;
	}

	public LocalDate getEndDate() {
		return endDate;
	}

	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Bike getBike() {
		return bike;
	}

	public void setBike(Bike bike) {
		this.bike = bike;
	}

	private Bike bike;
}
