package controller;

import java.time.LocalDate;

import entity.bike.Bike;
import entity.rent.Renting;

/**
 * @author SnowPhantom
 *
 */
public class ReturnBikeController implements BaseController {
	
	private Bike bike;
	private Renting renting;
	
	/**
	 * Get current renting information
	 * @return renting
	 */
	public Renting getRentingInfo() {
		return renting;
	}
	
	
	/**
	 * Conduct the return bike work
	 */
	public void returnBike() {
		renting.setRentingStatus(2);
		renting.setEndDate(LocalDate.now());
		renting.setCost(calculateFee(renting));
		
		bike.setBikeStatus(0);
	}
	
	/**
	 * Conduct calculating fee of the rent
	 * @param renting
	 * @return fee
	 */
	public int calculateFee(Renting renting) {
		return 0;
	}
}
