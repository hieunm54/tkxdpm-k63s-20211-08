package model;

import annotaion.*;

public class Bike {
	
	@Key
	private int BikeId;
	private String BikeName;
	@Unique
	private String BikeCode;
	private int BikeType;
	
	@CustomData
	private String BikeTypeName;

	private String Color;

	public Bike() {
	}

	public Bike(int bikeId, String bikeCode, String name, int bikeType, String bikeTypeName, String color) {
		super();
		BikeId = bikeId;
		BikeCode = bikeCode;
		BikeName = name;
		BikeType = bikeType;
		BikeTypeName = bikeTypeName;
		Color = color;
	}

	public int getBikeId() {
		return BikeId;
	}

	public void setBikeId(int bikeId) {
		BikeId = bikeId;
	}

	public String getBikeName() {
		return BikeName;
	}

	public void setBikeName(String name) {
		BikeName = name;
	}

	public int getBikeType() {
		return BikeType;
	}

	public void setBikeType(int bikeType) {
		BikeType = bikeType;
	}

	public String getBikeTypeName() {
		return BikeTypeName;
	}

	public void setBikeTypeName(String bikeTypeName) {
		BikeTypeName = bikeTypeName;
	}

	public String getColor() {
		return Color;
	}

	public void setColor(String color) {
		Color = color;
	}

	public String getBikeCode() {
		return BikeCode;
	}

	public void setBikeCode(String bikeCode) {
		BikeCode = bikeCode;
	}

	
	
}
