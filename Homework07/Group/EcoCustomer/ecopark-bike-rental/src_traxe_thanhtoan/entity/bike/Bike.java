package entity.bike;

public class Bike {
	private int bikeId;
	private String bikeName;
	private String bikeTypeName;
	private String bikeCode;
	private int bikeStatus;
	private int bikeType;
	private String batteryStatus; 
	
	public Bike(int bikeId, String bikeCode) {
		this.bikeId = bikeId;
		this.bikeCode = bikeCode;
	}
	
	public int getBikeId() {
		return bikeId;
	}
	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}
	public String getBikeName() {
		return bikeName;
	}
	public void setBikeName(String bikeName) {
		this.bikeName = bikeName;
	}
	public String getBikeTypeName() {
		return bikeTypeName;
	}
	public void setBikeTypeName(String bikeTypeName) {
		this.bikeTypeName = bikeTypeName;
	}
	public String getBikeCode() {
		return bikeCode;
	}
	public void setBikeCode(String bikeCode) {
		this.bikeCode = bikeCode;
	}
	public int getBikeType() {
		return bikeType;
	}
	public void setBikeType(int bikeType) {
		this.bikeType = bikeType;
	}
	
	void checkAvailable() {}
	public int getBikeStatus() {
		return bikeStatus;
	}
	public void setBikeStatus(int bikeStatus) {
		this.bikeStatus = bikeStatus;
	}

	public String getBatteryStatus() {
		return batteryStatus;
	}

	public void setBatteryStatus(String batteryStatus) {
		this.batteryStatus = batteryStatus;
	}
}
