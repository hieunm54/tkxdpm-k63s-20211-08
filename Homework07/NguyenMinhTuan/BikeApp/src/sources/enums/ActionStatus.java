package sources.enums;

public enum ActionStatus {
	UPDATE,
	ADD,
	DELETE
}