package model;

import annotaion.Key;

public class BikeType {

	@Key
	private int bikeType;

	private String bikeTypeName;
	
	public BikeType() {
		
	}

	public BikeType(int bikeTypeId, String bikeTypeName) {
		super();
		this.bikeType = bikeTypeId;
		this.bikeTypeName = bikeTypeName;
	}
	
	public String toString() {
		return bikeTypeName;
	}

	public int getBikeType() {
		return bikeType;
	}

	public void setBikeType(int bikeType) {
		this.bikeType = bikeType;
	}

	public String getBikeTypeName() {
		return bikeTypeName;
	}

	public void setBikeTypeName(String bikeTypeName) {
		this.bikeTypeName = bikeTypeName;
	}
	
	
}
