package model;

import annotaion.*;

public class Bike {
	
	@Key
	private int bikeId;
	private String bikeName;
	@Unique
	private String bikeCode;
	private int bikeType;
	
	@CustomData
	private String bikeTypeName;

	private String color;
	
	@CustomData
	public int Index;

	public Bike() {
	}

	public Bike(int bikeId, String bikeCode, String name, int bikeType, String bikeTypeName, String color) {
		super();
		this.bikeId = bikeId;
		this.bikeCode = bikeCode;
		this.bikeName = name;
		this.bikeType = bikeType;
		this.bikeTypeName = bikeTypeName;
		this.color = color;
	}

	public int getBikeId() {
		return bikeId;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}

	public String getBikeName() {
		return bikeName;
	}

	public void setBikeName(String bikeName) {
		this.bikeName = bikeName;
	}

	public String getBikeCode() {
		return bikeCode;
	}

	public void setBikeCode(String bikeCode) {
		this.bikeCode = bikeCode;
	}

	public int getBikeType() {
		return bikeType;
	}

	public void setBikeType(int bikeType) {
		this.bikeType = bikeType;
	}

	public String getBikeTypeName() {
		return bikeTypeName;
	}

	public void setBikeTypeName(String bikeTypeName) {
		this.bikeTypeName = bikeTypeName;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getIndex() {
		return Index;
	}

	public void setIndex(int index) {
		Index = index;
	}
	

}
