package views.screen;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import controller.BikeController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import model.Bike;
import model.BikeType;
import sources.enums.ActionStatus;

public class BikeControlHandler {

	public BikeControlHandler() {
		super();

		bikeController = new BikeController(Bike.class);
	}

	BikeController bikeController;
	List<Bike> bikes;
	Bike bike;

	@FXML
	private TableView<Bike> BikeTable;

	@FXML
	private TextField codeInput;

	@FXML
	private TextField colorInput;

	@FXML
	private TextField nameInput;

	@FXML
	private ChoiceBox<BikeType> typeInput;

	@FXML
	public void initialize() {
		System.out.println("Initialized");

		List<TableColumn<Bike, ?>> cols = BikeTable.getColumns();
		String[] fields = { "Index", "BikeCode", "BikeName", "Color", "BikeTypeName" };

		for (int i = 0; i < cols.size(); i++) {
			TableColumn<Bike, ?> col = cols.get(i);
			col.setCellValueFactory(new PropertyValueFactory<>(fields[i]));
		}

		List<BikeType> bikeTypes = bikeController.getBikeType();

		ObservableList<BikeType> bikeTypeData = FXCollections.observableArrayList(bikeTypes);

		typeInput.setItems(bikeTypeData);

		loadData();

	}

	@FXML
	void Add(ActionEvent event) {
		System.out.println("Add Action");
		Bike addBike = getInputValue();
		if (!validate(addBike, ActionStatus.ADD)) {
			return;
		}
		
		bikeController.add(addBike);
		loadData();
	}

	@FXML
	void Back(ActionEvent event) {
		System.out.println("Back Action");
	}

	@FXML
	void Delete(ActionEvent event) {
		System.out.println("Delete Action");

		if (bike == null) {
			return;
		}
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Thông báo");
		alert.setHeaderText("Bạn có chắc muốn xoá bản ghi!");
		alert.showAndWait().ifPresent(rs -> {
		    if (rs == ButtonType.OK) {
				bikeController.delete(bike.getBikeId());
				loadData();
				assignValue(bikes.get(0));
		    }
		});
	}

	@FXML
	void Update(ActionEvent event) {
		System.out.println("Update Action");
		Bike updateBike = getInputValue();
		updateBike.setBikeId(bike != null ? bike.getBikeId() : -1);
		
		if (!validate(updateBike, ActionStatus.UPDATE)) {
			return;
		}
		
		bikeController.update(updateBike);
		loadData();
		
		
	}

	@FXML
	void clickEvent(MouseEvent event) {
		System.out.println("Click");
		
		Bike clickBike = BikeTable.getSelectionModel().getSelectedItem();
		
		assignValue(clickBike);
	}

	/**
	 * Hàm load dữ liệu
	 */
	void loadData() {

		bikes = bikeController.get();
		ObservableList<Bike> list = FXCollections.observableArrayList(bikes);

		BikeTable.setItems(list);
	}

	/**
	 * Hàm lấy dữ liệu từ các ô input
	 * 
	 * @return
	 */
	Bike getInputValue() {
		Bike returnBike = new Bike();
		
		BikeType returnBikeType = typeInput.getValue();

		returnBike.setBikeCode(codeInput.getText());
		returnBike.setBikeName(nameInput.getText());
		returnBike.setBikeType(returnBikeType != null ? returnBikeType.getBikeType() : -1);
		returnBike.setColor(colorInput.getText());

		return returnBike;
	}
	
	/**
	 * 
	 * Hàm bind dữ liệu vào ô input
	 * @param assignBike
	 */
	void assignValue(Bike assignBike) {
		
		bike = assignBike;

		codeInput.setText(assignBike.getBikeCode());
		nameInput.setText(assignBike.getBikeName());
		colorInput.setText(assignBike.getColor());
		typeInput.setValue(new BikeType(assignBike.getBikeType(), assignBike.getBikeTypeName()));
	}
	
	/**
	 * Hàm validate dữ liệu
	 * @param validateBike
	 * @param actionStatus
	 * @return
	 */
	boolean validate(Bike validateBike, ActionStatus actionStatus) {
		
		if (!validateSimpleString(validateBike.getBikeCode())) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Thông báo");
			alert.setHeaderText("Mã xe không được để trống!");
			alert.showAndWait();
			return false;
		}
		
		if (!validateSimpleString(validateBike.getBikeName())) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Thông báo");
			alert.setHeaderText("Tên xe không được để trống!");
			alert.showAndWait();
			return false;
		}
		
		if (validateBike.getBikeType() == -1) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Thông báo");
			alert.setHeaderText("Vui lòng chọn loại xe!");
			alert.showAndWait();
			return false;
		}
		
		boolean duplicate = bikeController.validate(validateBike, actionStatus);
		
		if (!duplicate) {
			Alert alert = new Alert(AlertType.WARNING);
			alert.setTitle("Thông báo");
			alert.setHeaderText("Mã xe đã tồn tại trong hệ thống!");
			alert.showAndWait();
		}
		
		return duplicate;
	}
	
	/**
	 * Hàm validate chuỗi bình thường
	 * @param value
	 * @return
	 */
	public boolean validateSimpleString(String value) {
		
		if (value == null || value == "") {
			return false;
		}
		
		Pattern p = Pattern.compile ("[!@#$%&*()_+=|<>?{}\\[\\]~-]");
		Matcher m = p.matcher(value);
		
		if (m.find()) {
			return false;
		}
		
		
		return true;
	}
}
