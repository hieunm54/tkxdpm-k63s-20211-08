package view.handler;

import java.io.IOException;
import java.util.Hashtable;
import java.util.List;

import application.App;
import controller.BaseController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Config;

public class BaseHandler {
	protected static final Stage stage = App.stage;
	private FXMLLoader loader;
	private Scene scene;
	private BaseHandler prev;
//	protected HomeScreenHandler homeScreenHandler;
	protected Hashtable<String, String> messages;
	private BaseController bController;
	
	
	public BaseHandler(String screenPath) throws IOException {
		this.loader = new FXMLLoader(getClass().getResource(screenPath));
		
		// set an obj of this class as handler for a screen having screenPath
		this.loader.setController(this);
		
		this.scene = new Scene(loader.load());
	}
	
	
	public void setPreviousScreen(BaseHandler prev) {
		this.prev = prev;
	}

	public BaseHandler getPreviousScreen() {
		return this.prev;
	}

	public void show() {
		stage.setScene(this.scene);
		stage.show();
	}

	public void setBController(BaseController bController){
		this.bController = bController;
	}

	public BaseController getBController(){
		return this.bController;
	}

	public void forward(Hashtable messages) {
		this.messages = messages;
	}

//	public void setHomeScreenHandler(HomeScreenHandler HomeScreenHandler) {
//		this.homeScreenHandler = HomeScreenHandler;
//	}
}
