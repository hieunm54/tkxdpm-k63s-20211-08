package application;
	
import controller.PaymentController;
import entity.Payment;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.Config;
import view.handler.BaseHandler;
import view.handler.PaymentHandler;

/*
 * 1.Build path:
 * -> add all jar files in javafx/lib to Modules path, class path
 * 2.Set run configuration for MAIN METHOD: 
 * Arguments -> VM Arguments:
 * --module-path "lib\javafx-sdk\lib" --add-modules javafx.controls,javafx.fxml
 * 
 * 3.In each fxml file, add:
 *   + fx:controller="application.Controller"  for Container
 *   + fx:id="..." (must be same as declared in controller class)
 *   + onAction="#nameOfMethodDefinedInController" 
 */

public class App extends Application {
	public static Stage stage; //1.
	@Override
	public void start(Stage primaryStage) {
		try {
			stage = primaryStage; //2.
			BaseHandler paymentHandler = new PaymentHandler(Config.PAYMENT_SCREEN_PATH, 5000, 50, 5000); // tiền cọc, tiền đã thuê, tiền hoàn lại
			paymentHandler.setBController(new PaymentController());
			paymentHandler.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
